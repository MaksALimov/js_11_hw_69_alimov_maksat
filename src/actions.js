export const AXIOS_TV_REQUEST = 'AXIOS_TV_REQUEST';
export const ON_INPUT_CHANGE = 'ON_INPUT_CHANGE';
export const HIDE_SERIES_LINKS = 'HIDE_SERIES_LINKS';
export const SHOW_SERIES_NAME = 'SHOW_MOVIE_NAME';
export const SHOW_SERIES_IMAGE = 'SHOW_MOVIE_IMAGE';
export const SHOW_SERIES_SUMMARY = 'SHOW_MOVIE_SUMMARY';
export const SHOW_SERIES_PREMIERED = 'SHOW_MOVIE_PREMIERED';
export const SHOW_SERIES_GENRES = 'SHOW_MOVIE_GENRES';

export const axiosTvRequest = filteredShows => ({type: AXIOS_TV_REQUEST, payload: filteredShows});
export const onInputChange = text => ({type: ON_INPUT_CHANGE, payload: text});
export const hideSeriesLinks = () => ({type: HIDE_SERIES_LINKS});

export const showSeriesName = name => ({type: SHOW_SERIES_NAME, payload: name});
export const showSeriesImage = img => ({type: SHOW_SERIES_IMAGE, payload: img});
export const showSeriesSummary = summary => ({type: SHOW_SERIES_SUMMARY, payload: summary});
export const showSeriesPremiered = premiered => ({type: SHOW_SERIES_PREMIERED, payload: premiered});
export const showSeriesGenres = genres => ({type: SHOW_SERIES_GENRES, payload: genres});
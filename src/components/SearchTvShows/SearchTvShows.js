import React, {useEffect, useReducer} from 'react';
import './SearchTvShows.css';
import axios from "axios";
import {NavLink} from "react-router-dom";
import {
    AXIOS_TV_REQUEST,
    axiosTvRequest,
    HIDE_SERIES_LINKS,
    hideSeriesLinks,
    ON_INPUT_CHANGE,
    onInputChange
} from "../../actions";

const initialState = {
    tvSeries: [],
    inputText: '',
}

const reducer = (state, action) => {
    switch (action.type) {

        case AXIOS_TV_REQUEST: {
            return {...state, tvSeries: action.payload};
        }

        case ON_INPUT_CHANGE: {
            return {...state, inputText: action.payload};
        }

        case HIDE_SERIES_LINKS: {
            return {...state, inputText: ''};
        }

        default:
            return state;
    }
};

const SearchTvShows = () => {

    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const tvShowsData = async () => {
            const response = await axios.get('http://api.tvmaze.com/search/shows?q=csi');
            const filteredTvShows = response.data.map(show => {
                return {title: show.show.name, id: show.show.id}
            });
            dispatch(axiosTvRequest(filteredTvShows));
        };

        tvShowsData().catch(console.error);
    }, []);

    return (
        <div className="SearchWrapper">
            <h2>Search for TV Show</h2>
            <input type="text" value={state.inputText} onChange={e => dispatch(onInputChange(e.target.value))}/>
            <ul>
                {state.inputText.length === 3 ? state.tvSeries.filter(show => show.title.toLowerCase().includes(state.inputText.toLowerCase())).map(show => (
                    <NavLink key={show.id} to={`/shows/${show.id}`}>
                        <li onClick={() => dispatch(hideSeriesLinks())}>{show.title}</li>
                    </NavLink>
                )) : null}
            </ul>
        </div>
    );
};

export default SearchTvShows;
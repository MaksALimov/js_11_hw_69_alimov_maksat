import React from 'react';
import Layout from "./containers/Layout/Layout";
import SearchTvShows from "./components/SearchTvShows/SearchTvShows";
import {Route} from "react-router-dom";
import CurrentShow from "./containers/CurrentShow/CurrentShow";

const App = () => (
    <>
        <Layout>
            <SearchTvShows/>
            <Route path={'/shows/:id'} component={CurrentShow}/>
        </Layout>
    </>
);

export default App;
import React from 'react';
import './Layout.css';

const Layout = ({children}) => {
    return (
        <>
            <div className="LayoutWrapper">
                <h1 className="LayoutWrapper__title">TV Shows</h1>
            </div>
            {children}
        </>
    );
};

export default Layout;
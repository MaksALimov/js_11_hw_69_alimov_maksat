import React, {useEffect, useReducer} from 'react';
import axios from "axios";
import './CurrentShow.css';
import {
    SHOW_SERIES_IMAGE,
    SHOW_SERIES_NAME,
    SHOW_SERIES_PREMIERED,
    SHOW_SERIES_GENRES,
    SHOW_SERIES_SUMMARY, showSeriesGenres, showSeriesImage,
    showSeriesName, showSeriesPremiered, showSeriesSummary
} from "../../actions";

const initialState = {
    name: '',
    premiered: '',
    summary: '',
    image: '',
    genres: [],
}

const reducer = (state, action) => {
    switch (action.type) {
        case SHOW_SERIES_NAME: {
            return {...state, name: action.payload};
        }

        case SHOW_SERIES_IMAGE: {
            return {...state, image: action.payload};
        }

        case SHOW_SERIES_SUMMARY: {
            return {...state, summary: action.payload};
        }

        case SHOW_SERIES_PREMIERED: {
            return {...state, premiered: action.payload};
        }

        case SHOW_SERIES_GENRES: {
            return {...state, genres: action.payload};
        }

        default:
            return state;
    }
};

const CurrentShow = ({match}) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const showData = async () => {
            const response = await axios.get(`https://api.tvmaze.com/shows/${match.params.id}`);
            dispatch(showSeriesName(response.data.name));
            dispatch(showSeriesImage(response.data.image.original));
            //response.data.summary в ответе вернул мне теги как строки "<b></b>" с помощьюрегулярного выражения я их удалил
            dispatch(showSeriesSummary(response.data.summary.replace(/<\/?[^>]+(>|$)/g, "")));
            dispatch(showSeriesPremiered(response.data.premiered));
            dispatch(showSeriesGenres(response.data.genres));
        };

        showData().catch(console.error);
    }, [match.params.id]);

    return (
        <div className="CurrentShow">
            <h2 className="CurrentShow__title">{state.name}</h2>
            <img className="CurrentShow__img" src={state.image} alt="moviePoster"/>
            <p className="CurrentShow__description">{state.summary}</p>
            <p className="CurrentShow__premiered">Premiered: {state.premiered}</p>
            <h4 className="CurrentShow__genres-title">Genres: </h4>
            {state.genres.map(genres => (
                <span className="CurrentShow__genres">{genres}</span>
            ))}
        </div>
    );
};

export default CurrentShow;